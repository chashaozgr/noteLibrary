#coding=utf-8
# 本题为考试多行输入输出规范示例，无需提交，不计分。
import sys

if __name__ == "__main__":
    # 读取第一行的n
    k = int(sys.stdin.readline().strip())
    a = sys.stdin.readline().strip()
    b = sys.stdin.readline().strip()
    
    sub_a = []
    idx = 0
    while idx < len(a)-k+1:
    	inp = a[idx:idx+k]
    	if inp not in sub_a:
        	sub_a.append(a[idx:idx+k])
        idx = idx + 1

    count = 0
    for comp in sub_a:
    	idx = 0
    	while idx < len(b)-k+1:
    		if comp == b[idx:idx+k]:
    			count = count + 1
    		idx = idx + 1
    print count