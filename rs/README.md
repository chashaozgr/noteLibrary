# 推荐系统模块
单纯的想把一些所学内容给补上


## TODO

- [x] 协同过滤（CF）
	- [x] ITEM-BASED
	- [x] USER-BASED
- [ ] 特征工程尝试
	- [ ] ONEHOT基本功能
		- [ ] ~~data_preprocess 数据预处理~~，**由于此处预处理工作较少，所以就不需要了**
		- [x] fea_extract 特征提取
		- [x] gen_data 数据集构建
		- [ ] 必要的工作函数化
		- [ ] 避免出现不存在于onehot词典的现象
	- [ ] default特征引入
		- [ ] default特征定义
	- [ ] embedding信息加入
		- [ ] 哪些特征可以embedding
	- [ ] 其他特征
		- [ ] 历史观看信息参照
		- [ ] 相似电影
		- [ ] 相似人
		- [ ] 构造协同过滤特征
- [ ] LR模型的基本
	- [ ] sklearn版本
	- [ ] TF版本
	- [ ] 其他版本？（Keras？tf.keras？）
- [ ] FM和FFM系列
	- [ ] TF版本
- [ ] 更加强大的深度学习模型