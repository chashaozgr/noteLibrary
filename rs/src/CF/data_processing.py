import pandas as pd

MOVIE_PATH = "../../data/ml-20m/movies.csv"
RATING_PATH = "../../data/ml-20m/ratings.csv"
MOVIE_RATING_PATH = "../../data/movie_rating_20190219_1.csv"

# -----------------------------第一手数据处理---------------------------

# 读取数据
movies = pd.read_csv(MOVIE_PATH)
rating = pd.read_csv(RATING_PATH)

# 数据合并
data = pd.merge(movies, rating, on="movieId")

# 信息组合
data[['userId', 'rating', 'movieId', 'title']].sort_values('userId').to_csv(
    '../../data/movie_rating_20190219_1.csv', index=False)

# -----------------------------第一手数据处理---------------------------