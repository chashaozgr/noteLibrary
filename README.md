# noteLibrary
公开的个人学习笔记与代码库

## 内容简介
- dataMining：数据挖掘模块，主要涉及数据挖掘，数据分析等内容
    + dataProessing：数据处理，主要是预处理的简单应用
    + network_spider：简单网络爬虫笔记
- RLearning：R语言学习，过去进行R语言集中学习的笔记
- extra：平时自己的玩耍的骚操作
- TF-learning：tensorflow学习笔记（建设中）
- AlgoStructure：数据结构与算法笔记与练习实践

## 更新记录：
**2018-1**

- 创建“dataprocessing”，用于存放数据分析和处理方面的基本内容
- sklearn加载数据集与简单数据展示
- R语言学习笔记转移完成

**2018-2**

- extra文件夹新建，内含微信自动群发程序
- 创建TF-learning，tensorflow学习笔记，GAN模型实现
- 创建dataprocessing\\network\_spider，python爬虫学习笔记完成
- 创建AlgoStructure，排序算法笔记

**2018-3**

- Python复习笔记建立，面向对象笔记完成，小技巧学习笔记补充
- 删除数据结构预算法一章，内部内容全部转到Python复习笔记里面

![个人简介名片](https://gitee.com/chashaozgr/noteLibrary/raw/master/desktopPic/selfIntroduction.JPG)
