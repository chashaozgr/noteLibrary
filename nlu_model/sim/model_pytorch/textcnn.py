import torch
import torch.nn as nn
from torch.nn import functional as F
import torch.utils.data as torch_data_util
import math
from loguru import logger

class TextCNN(nn.Module):
    def __init__(self, param):
        super(TextCNN, self).__init__()
        ci = 1  # input chanel size
        kernel_num = param['kernel_num'] # output chanel size
        kernel_size = param['kernel_size']
        vocab_size = param['vocab_size']
        embed_dim = param['embed_dim']
        dropout = param['dropout']
        class_num = param['class_num']
        self.param = param
        self.embed = nn.Embedding(vocab_size, embed_dim, padding_idx=1)
        # pretrained_weight = np.array(param['pretrained_weight'])
        self.embed.weight = nn.Parameter(torch.FloatTensor(param['pre_word_embeds']))
        self.convs = [nn.Conv2d(ci, kernel_num, (i, embed_dim)) for i in kernel_size]
        self.dropout = nn.Dropout(dropout)
        # self.fc1 = nn.Linear(len(kernel_size) * kernel_num, param["output_vec"])
        self.fc1 = nn.Linear(448, param["output_vec"])

    def init_embed(self, embed_matrix):
        self.embed.weight = nn.Parameter(torch.Tensor(embed_matrix))

    @staticmethod
    def conv_and_pool(x, conv):
        # x: (batch, 1, sentence_length,  )
        x = conv(x)
        # x: (batch, kernel_num, H_out, 1)
        x = F.relu(x.squeeze(3))
        # x: (batch, kernel_num, H_out)
        x = F.avg_pool1d(x, 3)
        # logger.info(x.squeeze(2))
        # logger.info(torch.flatten(x, start_dim=1))
        x = torch.flatten(x, start_dim=1)
        #  (batch, kernel_num)
        return x

    def forward(self, x):
        # x: (batch, sentence_length)
        x = self.embed(x)
        x = x.unsqueeze(1)
        x_conv = [self.conv_and_pool(x, conv) for conv in self.convs]
        x_conv = torch.cat(x_conv, 1)  # (batch, 3 * kernel_num)
        fc_output = self.fc1(x_conv)
        output_vec = F.normalize(fc_output, p=2, dim=1)
        return output_vec

    def init_weight(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                m.weight.data.normal_(0, 0.01)
                m.bias.data.zero_()
            else:
                m.weight.data.normal_(0, 0.01)
                m.bias.data.zero_()