import jieba
from tensorflow import keras
from nlu_model.cls.model.textcnn_small import TextCNNSmall

class ClsModel(object):
    """docstring for ClsModel"""
    def __init__(self, model_choice, model_conf={}, train_conf={}):
        self.model_choice = model_choice
        self.model_conf = model_conf
        self.train_conf = train_conf
        self.__model_select__()

    def __model_select__(self):
        if self.model_choice == "textcnn_small":
            self.model = TextCNNSmall(self.model_conf, self.train_conf)
        if self.model_choice == "load":
            self.load(self.model_conf["path"])

    def preprocess(self, sentences):
        sentences = [list(jieba.cut(i)) for i in sentences]
        sentence_id = self.model_conf["emb_model"].batch2idx(sentences)
        return keras.preprocessing.sequence.pad_sequences(sentence_id,
                                                          value=0,
                                                          padding='post',
                                                          maxlen=50)

    def fit(self, x_train, y_train, x_test, y_test):
        return self.model.fit(x_train, y_train, x_test, y_test)

    def evaluate(self, x_test, y_test):
        return self.model.evaluate(x_test, y_test)

    def pred(self, sentence):
        return self.model.predict(sentence)

    def predict(self, sentences):
        sentence_id = self.preprocess(sentences)
        return self.pred([sentence_id])[0][0]

    def save(self, path):
        self.model.save(path)

    def load(self, path):
        self.model = keras.models.load_model(path)


        