import pickle

def save_pkl(path, data):
	output = open(path, 'wb')
	pickle.dump(data, output)
	output.close()

def load_pkl(path):
	pkl_file = open(path, 'rb')
	data = pickle.load(pkl_file)
	pkl_file.close()
	return data