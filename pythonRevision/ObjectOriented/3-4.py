class Programer(object):
    # 无限递归1000次
    # 1.
    # 设置对象属性
    # __setattr__(self, name, value)
    #
    # def __setattr__(self, name, value):
    #     self.__dict__[name] = value
    #
    # 2.
    # 查询对象属性
    # __getattr__(self, name)
    # 当查询不到时才会调用
    # __getattribute__(self, name)
    # 每次访问时一定会被调到
    # 3.
    # 删除对象属性
    # __delattr__
    def __init__(self,name,age):
        # print('call init')
        self.name=name
        self.age=age

    def __getattribute__(self, item):
        # print(item)
        # 避免无限递归
        return super(Programer,self).__getattribute__(item)

    def __setattr__(self, key, value):
        # print('__getattr__')
        self.__dict__[key]=value

programmer1=Programer('A',25)
print(programmer1.name)
