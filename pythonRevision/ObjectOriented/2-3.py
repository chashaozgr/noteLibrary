class Programer(object):
    hobby='Computer'

    def __init__(self,name,age,weight):
        self.name=name
        self._age=age
        self.__weight=weight

    @classmethod
    def get_hobby(cls):
        return cls.hobby

    @property
    def get_weight(self):
        return self.__weight

    def self_introduction(self):
        print('my name is %s \nI am %s years old\n' % (self.name,self._age))

# python的私有性有点特点
prog=Programer('A',25,80)
print(dir(prog))
print(Programer.get_hobby())
print(prog.get_weight)
prog.self_introduction()
