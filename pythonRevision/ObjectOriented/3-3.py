class Programer(object):
    # __str__
    # __repr__
    # __unicode__

    def __init__(self,name,age):
        # print('call init')
        self.name=name
        self.age=age

    def __str__(self):
        return 'you are using new str: name:%s, age:%s'%(self.name,self.age)

    def __dir__(self):
        return self.__dict__.keys()

programmer1=Programer('A',25)
print(programmer1)
print(dir(programmer1))
