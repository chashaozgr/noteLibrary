class Programer(object):
    hobby='Computer'

    def __init__(self,name,age,weight):
        self.name=name
        self._age=age
        self.__weight=weight

    @classmethod
    def get_hobby(cls):
        return cls.hobby

    @property
    def get_weight(self):
        return self.__weight

    def self_introduction(self):
        print('my name is %s \nI am %s years old\n' % (self.name,self._age))

class BackendProgrammer(Programer):

    def __init__(self,name,age,weight,language):
        super(BackendProgrammer,self).__init__(name,age,weight)
        self.language=language

    def self_introduction(self):
        print('%s就是这么任性'%(self.name))

def introduction(programmer):
    if isinstance(programmer,Programer):
        programmer.self_introduction()

programmer=BackendProgrammer('A',1,2,'Python')
programmer1=Programer('A',1,2)
print(dir(programmer))
print(programmer.__dict__)
programmer.self_introduction()
print('----------------')
introduction(programmer)
introduction(programmer1)
