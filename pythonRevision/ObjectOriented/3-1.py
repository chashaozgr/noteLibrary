class Programer(object):
    hobby='Computer'

    def __init__(self,name,age):
        print('call init')
        self.name=name
        self.age=age

    def __new__(cls, *args, **kwargs):
        print('call new')
        print(*args)
        print(**kwargs)
        # python2用这种
        # return super(Programer,cls).__new__(cls, *args, **kwargs)
        # python3用这种
        return super(Programer,cls).__new__(cls)

programmer=Programer('A',25)
print(programmer.__dict__)