class Programer(object):
    hobby='Computer'

    def __init__(self,name,age,weight):
        self.name=name
        self._age=age
        self.__weight=weight

    def get_weight(self):
        return self.__weight

# python的私有性有点特点
prog=Programer('A',25,80)
print(dir(prog))
print(prog._age)
print(prog.__weight)
print(prog._Programer__weight)
print(prog.get_weight())