class OldStyle:

    def __init__(self,name,description):
        self.name=name
        self.description=description

class NewStyle(object):

    def __init__(self,name,description):
        self.name=name
        self.description=description

oldStyle=OldStyle('old','1')
newStyle=NewStyle('new','2')


# 在python3两种定义都类似，推进新的那种
print(oldStyle)
print(type(oldStyle))
print(dir(oldStyle))
print('--------------')
print(newStyle)
print(type(newStyle))
print(dir(newStyle))