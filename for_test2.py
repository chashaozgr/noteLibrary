#!/bin/python
# -*- coding: utf8 -*-
import sys
import os
import re

# 请完成下面这个函数，实现题目要求的功能
# 当然，你也可以不按照下面这个模板来作答，完全按照自己的想法来 ^-^
#******************************开始写代码******************************


def change(str1, str2):
    calchange = 0
    if len(str1) == len(str2):
        for i in range(len(str1)):
            if str1[i] != str2[i]:
                calchange = calchange + 1
    else:
        shorter = ''
        longer = ''
        if len(str1) < len(str2):
            longer = [i for i in str2]
            shorter = [i for i in str1]
        else:
            longer = [i for i in str1]
            shorter = [i for i in str2]
        while True:
            for i in shorter:
                if i not in longer:
                    continue
            
        calchange = len(longer)

    return calchange

#******************************结束写代码******************************


try:
    _str1 = raw_input()
except:
    _str1 = None

try:
    _str2 = raw_input()
except:
    _str2 = None


res = change(_str1, _str2)

print str(res) + "\n"
