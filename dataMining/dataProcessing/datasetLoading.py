###
# 更新时间：2018年1月12日
# 更新内容：数据及加载和基本数据展示
###

# sklearn数据集加载
from sklearn import datasets
import matplotlib.pyplot as plt

# 数据及加载
iris=datasets.load_iris()
print(iris)

# 计数
countTargetSetosa=0
countTargetVersicolor=0
countTargetVirginica=0
for irisClass in iris['target']:
    if irisClass==0:
        countTargetSetosa=countTargetSetosa+1
    elif irisClass==1:
        countTargetVersicolor=countTargetVersicolor+1
    elif irisClass==2:
        countTargetVirginica=countTargetVirginica+1
    else:
        print('发现异常数据：'+str(irisClass))

# 数据可视化
dataShow=[countTargetSetosa,countTargetVersicolor,countTargetVirginica]
dataLabel=['Setosa','Versicolor','Virginica']
plt.bar(range(len(dataShow)),dataShow,tick_label=dataLabel,color='rgb')
plt.show()

