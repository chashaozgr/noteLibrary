#!/bin/python
# -*- coding: utf8 -*-
import sys
import os
import re

#请完成下面这个函数，实现题目要求的功能
#当然，你也可以不按照下面这个模板来作答，完全按照自己的想法来 ^-^ 
#******************************开始写代码******************************
def change(st):
    maxIdx = 0
    dic = {}
    outSt = []
    for i in st:
        if dic.has_key(i):
            outSt.append(dic[i])
        else:
            dic[i] = maxIdx
            maxIdx = maxIdx + 1
            outSt.append(dic[i])
    return outSt

def  solve(S, T):
    lenModel = len(T)
    count = 0
    modelT = change(T)
    for i in range(len(S)-lenModel+1):
        modelS_tmp = change(S[i:i+lenModel])
        if modelT == modelS_tmp:
            count = count +1
    return count



#******************************结束写代码******************************


try:
    _S = raw_input()
except:
    _S = None

try:
    _T = raw_input()
except:
    _T = None

  
res = solve(_S, _T)

print str(res) + "\n"