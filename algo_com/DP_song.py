# coding:utf-8
# 小Q的歌单
def match(a, x, b, y, k):
    get = []
    for i in range(x + 1):
        for j in range(y + 1):
            if i * a + j * b == k:
                get.append([i, j])
    return get


def c(i, j):
    a = 1
    b = 1
    tmp_i = i
    tmp_j = j
    while tmp_i >= 1:
        a = a * tmp_j
        b = b * tmp_i
        tmp_i = tmp_i - 1
        tmp_j = tmp_j - 1
    return a / b


k = int(raw_input().strip())
# print(c(2,4))
[a, x, b, y] = [int(i) for i in raw_input().strip().split()]
get = match(a, x, b, y, k)
if len(get) == 0:
    print 0
else:
    result = 0
    for i in get:
        result = result + c(i[0], x) * c(i[1], y)
    print result % 1000000007
