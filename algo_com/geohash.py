# coding:utf8

def geohash(a):
	code = []
	lower = -90
	higher = 90
	if a >= 0:
		for i in range(6):
			mid = (lower+higher)/2
			if a >= mid:
				code.append("1")
				lower = mid
			else:
				code.append("0")
				higher = mid
	if a < 0:
		for i in range(6):
			mid = (lower+higher)/2
			if a > mid:
				code.append("1")
				lower = mid
			else:
				code.append("0")
				higher = mid
	return "".join(code)

a = int(raw_input().strip())
print geohash(a)