# coding:utf8
# 贪吃的小Q
import math
def sumGet(s,n,m):
	# 第一天吃s，则到第n天一共要吃多少？
	res = 0
	tmp_eat = float(s)
	for i in range(n):
		res = res + tmp_eat
		tmp_eat =  math.ceil(tmp_eat / 2)
		if res > m:
			return m+1
	return res

def cal_res(m,n):
	if n == 1:
		return m
	low = 1
	high = m
	while (low<high):
		mid = math.ceil((low+high)/2)
		tmp_cal = sumGet(mid, n, m)
		if tmp_cal == m:
			return mid
		elif tmp_cal > m:
			high = mid
		elif tmp_cal < m:
			low = mid
	return high

n,m = [int(i) for i in raw_input().strip().split(' ')]
# n day, m piece of candy
print int(cal_res(m,n))