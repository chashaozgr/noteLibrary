# coding:utf-8
# 腾讯 编码
def isZ(a):
	# 是否是质数
	if a == 1:
		return 0
	for i in range(a):
		if i == 0 or i == 1:
			continue
		if a % i == 0:
			return 0
	return 1

def cal(a):
	count = 0
	for i in range(a/2):
		if i <= 1:
			continue
		if isZ(i) and isZ(a-i):
			count = count + 1
	if isZ(a/2) and isZ(a-a/2):
		count = count + 1
	return count

inp = int(raw_input().strip())
print cal(inp)